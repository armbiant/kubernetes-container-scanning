# Log4j Kubernetes Container Scanning

## Prerequisites

A quick way to bring up a Kubernetes cluster is in Civo Cloud. Create an account, and follow the documentation on [how to set up the CLI](https://www.civo.com/learn/kubernetes-cluster-administration-using-civo-cli) with an API token. Next, create a k3s cluster. 

```shell
$ civo kubernetes create log4j
$ civo kubernetes config log4j --save
$ kubectl config use-context log4j
$ kubectl get node
```

`registry.gitlab.com/gitlab-de/playground/log4shell-vulnerable-app:latest` provides a vulnerable container image we can deploy and then scan.

### Deploy the app

```shell
$ cd kubernetes

$ kubectl apply -f deployment.yaml

$ kubectl apply -f service.yaml
```


### Tests

Test the application container with port forwarding, and open your browser at http://localhost:80808`. You can close the connection with `ctrl+c`. 

```
$ kubectl port-forward service/log4j 8080:8080
```

### Install Kubernetes Cluster Scanning

Follow the [documentation](https://docs.gitlab.com/ee/user/application_security/cluster_image_scanning/) and make sure that the [Starboard Operator](https://aquasecurity.github.io/starboard/v0.13.1/operator/installation/kubectl/) is installed before continuing. 


